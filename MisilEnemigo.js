﻿var explocion:Transform;
var sonidoMisil:AudioClip;
var velocidadProyectil:float=5;
var dano:int=100;

function Start () {

}

function Update () {
transform.Translate(Vector3(0,0,1)*Time.deltaTime*velocidadProyectil);
}
function OnCollisionEnter(col: Collision)
{
if(col.gameObject.tag == "Player")
	{
	Jugador.vida=Jugador.vida-dano;
	} 
Instantiate(explocion,transform.position,transform.rotation);
GetComponent.<AudioSource>().PlayClipAtPoint(sonidoMisil,transform.position);
Destroy(gameObject);
}
