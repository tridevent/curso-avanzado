﻿#pragma strict
var vida:int=3;
var velocidad:int=15;
var avanzar:boolean=true;
//-----Efecto Explosion----//
var copia:Transform;
var explosion:Transform;
var motor:GameObject;

function Start () {
avanzar=true;
vida=3;
}

function Update () {
if(vida>0)
	{
	transform.Translate(Vector3(0,0,1)*Time.deltaTime* velocidad);
	}

}
function OnTriggerEnter (col:Collider)
{
if(col.gameObject.tag=="AreaDisparo")
	{
	avanzar=false;
	}
}
function OnTriggerExit (col:Collider)
{
if(col.gameObject.tag=="AreaDisparo")
	{
	avanzar=true;
	}
}
function OnCollisionEnter(other:Collision)
{
if(other.gameObject.tag=="bala")
	{
	vida--;
	if(vida<=0)
		{
		Instantiate(explosion,motor.transform.position,transform.rotation);
		Instantiate(copia,transform.position,transform.rotation);
		Destroy(gameObject);
		}
	}
}