var proyectil: Rigidbody[];
var sonidoDisparo:AudioClip;
var sonidoRecarga:AudioClip;
var velocidadProyectil:float[];
static var municion:int=20;
var municionActual:int[];
var municionTotal:int[];
var rotacion:GameObject;
var humo:Transform;
private var espera:float=50;
static var recargando:boolean;
static var armaActual:int=0;
private var totalArmas:int=0;
static var cambiarArma:boolean=false;
function Awake()
{
municion = municionActual[armaActual];
}
function Start()
{
recargando=false;
for(var a:int=0; a < municionActual.length; a++)
	{
	municionTotal[a]= municionActual[a];
	}
for(var i:int=0; i < velocidadProyectil.length; i++)
	{
	totalArmas+=1;
	}
totalArmas-=1;
}
function Update()
{
if (Input.GetButtonUp("cambioArma"))
	{
	cambiarArma=true;
	if (armaActual < totalArmas)
		{	
		armaActual+=1;
		print("mejor arma");
		print(armaActual);
		}
	else
		{
		armaActual-=1;
		print("peor arma");
		}
	municion = municionActual[armaActual];	
	}
if(Input.GetMouseButtonUp(0)/*Input.GetButtonDown("disparo")*/&& municion<=0)
	{
	GetComponent.<AudioSource>().PlayOneShot(sonidoRecarga);
	recargando=true;
	//print("sonido");
	cambiarArma=false;
	}
if(Input.GetMouseButtonUp(0)/*Input.GetButtonDown("disparo")*/&& municion>0)
	{
	cambiarArma=false;
	var bala:Rigidbody=Instantiate(proyectil[armaActual],transform.position,rotacion.transform.rotation);
	bala.velocity=transform.TransformDirection(Vector3(0,0,velocidadProyectil[armaActual]));
	Physics.IgnoreCollision(bala.GetComponent.<Collider>(),transform.root.GetComponent.<Collider>());
	GetComponent.<AudioSource>().PlayOneShot(sonidoDisparo);
	Instantiate(humo,transform.position,transform.rotation);
	municion --;
	municionActual[armaActual]--;	
//	print(municion);
	}
if(municion<=0 && recargando==true)
	{
	espera--;
	if(espera==0)
		{
		espera=50;
		//municion=municionTotal[armaActual];
		recarganado=false;
		}
	}
}

function OnGUI()
{
GUI.Label (Rect(100,100,160,30),"Municion:"+municion);
}
@script RequireComponent(AudioSource)